package xmlparser;

import java.awt.EventQueue;
import java.io.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;



import org.w3c.dom.Node;
import org.w3c.dom.Element;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;

public class SOAPClient11 {
	 String SOAP_ENDPOINT;
	 String SOAP_ACTION;
	 String TARGET_NAMESPACE;
	 String OPERATION_NAME;
	 String PARAMETER_NAME;
	 String PARAMETER_VALUE;
	 
	private JFrame frame;
	private static JTextField textField;
	public DefaultTableModel model;
	public String ad;
	public ButtonGroup grup1;
	private JScrollPane scrollPane;
	JRadioButton rdbtnBorcsorgu;
	JRadioButton rdbtnTahsilatsorgu;
	private JTable table_1;
	public SOAPClient11() { 
		
		initialize();
		
	}
	public SOAPClient11(String endpoint, String namespace, String operation, String parameter, String value) {
    	SOAP_ENDPOINT = endpoint;
    	SOAP_ACTION = namespace + operation;
    	TARGET_NAMESPACE = namespace;
    	OPERATION_NAME = operation;
    	PARAMETER_NAME = parameter;
    	PARAMETER_VALUE = value;
    }
	
	 public Document loadXMLFromString(String xml) throws Exception
	    {
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        InputSource is = new InputSource(new StringReader(xml));
	        return builder.parse(is);
	    }
	 public String callSoapWebService() {
   	  String ak = null;
   	try {
           // Create SOAP Connection
           SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
           SOAPConnection soapConnection = soapConnectionFactory.createConnection();

           // Send SOAP Message to SOAP Server
           SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), SOAP_ENDPOINT);

  
           ByteArrayOutputStream stream = new ByteArrayOutputStream();
           soapResponse.writeTo(stream);
           ak=(new String(stream.toByteArray()));
           

           soapConnection.close();
       } catch (Exception e) {
           System.err.println("\nError occurred while sending SOAP Request to Server: " + e.getMessage());
           e.printStackTrace();
       }
     
		return ak;
   }
	 public SOAPMessage createSOAPRequest() throws Exception {
	        MessageFactory messageFactory = MessageFactory.newInstance();
	        SOAPMessage soapMessage = messageFactory.createMessage();

	        createSoapEnvelope(soapMessage);

	        MimeHeaders headers = soapMessage.getMimeHeaders();
	        headers.addHeader("SOAPAction", SOAP_ACTION);

	        soapMessage.saveChanges();



	        return soapMessage;
	    }

	    public void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
	        String namespace = "namespace";
	        SOAPPart soapPart = soapMessage.getSOAPPart();

	        // SOAP Envelope
	        SOAPEnvelope envelope = soapPart.getEnvelope();
	        envelope.addNamespaceDeclaration(namespace, TARGET_NAMESPACE);

	        // SOAP Body
	        SOAPBody soapBody = envelope.getBody();
	        SOAPElement soapBodyElem = soapBody.addChildElement(OPERATION_NAME, namespace);
	        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(PARAMETER_NAME, namespace);
	        soapBodyElem1.addTextNode(PARAMETER_VALUE);
	    }

	/**
	 * Launch the application.
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SOAPClient11 window = new SOAPClient11();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(184, 134, 11));
		frame.setBounds(100, 100, 696, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JLabel lblNewLabel =new JLabel(); 
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 14));
		lblNewLabel.setBounds(329, 39, 188, 28);
		frame.getContentPane().add(lblNewLabel);
		JLabel lblReferansno = new JLabel("ReferansNo:");
		lblReferansno.setForeground(Color.WHITE);
		lblReferansno.setFont(new Font("Arial", Font.BOLD, 14));
		lblReferansno.setBounds(10, 11, 92, 22);
		frame.getContentPane().add(lblReferansno);
		grup1=new ButtonGroup();
		textField = new JTextField();
		textField.setBounds(104, 13, 99, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		rdbtnBorcsorgu = new JRadioButton("BorcSorgu");
		
		rdbtnBorcsorgu.setBounds(292, 12, 109, 23);
		grup1.add(rdbtnBorcsorgu);
	
		frame.getContentPane().add(rdbtnBorcsorgu);
		
		rdbtnTahsilatsorgu = new JRadioButton("TahsilatSorgu");
		rdbtnTahsilatsorgu.setBounds(436, 12, 109, 23);
		grup1.add(rdbtnTahsilatsorgu);
		frame.getContentPane().add(rdbtnTahsilatsorgu);
		JButton btnNewButton = new JButton("\u00C7a\u011F\u0131r");
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 14));
		btnNewButton.setBackground(Color.ORANGE);
		btnNewButton.setBounds(104, 44, 99, 23);
		frame.getContentPane().add(btnNewButton);
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(13,73, 660, 247);
		frame.getContentPane().add(scrollPane);
		table_1 = new JTable();
	     scrollPane.add(table_1);
	    
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			
				String endpoint = "http://81.214.73.178/TahsilatService/TahsilatService.asmx";
		        String namespace = "http://tempuri.org/";
		        String operation = "BorcSorgu";
		        String operation1="TahsilatSorgu";
		        String parameter = "referansNo";
		        String value = textField.getText();
		        //String value = "58302861634"
		        //String value = "30295872165"
		        SOAPClient11 client = new SOAPClient11(endpoint, namespace, operation, parameter, value);
		        SOAPClient11 client1=new SOAPClient11(endpoint,namespace,operation1,parameter,value);
		     
		   
		        Document doc = null;
		    	if(rdbtnBorcsorgu.isSelected()) {
		    		model=new DefaultTableModel(); 
		    		table_1.setModel(model);
		    		 scrollPane.setViewportView(table_1);
		    		model.addColumn("BorcReferansNo");
		    		model.addColumn("GelirID");
		    		model.addColumn("BorcTur");
		    		model.addColumn("DonemTaksit");
		    		model.addColumn("SonOdemeTarih");
		    		model.addColumn("Tutar");
		    		model.addColumn("Gecikme");
		    		model.addColumn("Toplam");
		    		
		    	
		    	  try {
						doc = client.loadXMLFromString(client.callSoapWebService());
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        doc.getDocumentElement().normalize();
		    	  
		    	  NodeList nList = doc.getElementsByTagName("BorcDetay");
		      
		      		lblNewLabel.setText(doc.getElementsByTagName("AdSoyad").item(0).getTextContent());
		        for (int temp = 1; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		        
		            
		            if (nNode.getNodeType() == Node.ELEMENT_NODE 	) {
		               Element eElement = (Element) nNode;
		         
		               			String b_no=eElement.getElementsByTagName("BorcReferansNo").item(0).getTextContent();
		               			String g_ID=eElement.getElementsByTagName("GelirID").item(0) .getTextContent();
		         				String b_tur=eElement.getElementsByTagName("BorcTur").item(0).getTextContent();
		         				String d_taksit=eElement.getElementsByTagName("DonemTaksit").item(0).getTextContent();
		         				String S_tarih=eElement.getElementsByTagName("SonOdemeTarih").item(0).getTextContent();
		         				String tutar=eElement.getElementsByTagName("Tutar").item(0).getTextContent();
		         				String gecikme=eElement.getElementsByTagName("Gecikme").item(0).getTextContent();
		         				String toplam=eElement.getElementsByTagName("Toplam").item(0).getTextContent();
		         			//	ad=elm.getElementsByTagName("AdSoyad").item(0).getTextContent();
		         				model.insertRow(model.getRowCount(), new Object [] {b_no,g_ID,b_tur,d_taksit,S_tarih,tutar,gecikme,toplam});
		         				 
		            }
		           
		         }
		      
		    	}	 
		      
		     if(rdbtnTahsilatsorgu.isSelected()) {
		    	 model=new DefaultTableModel(); 
		    	 table_1.setModel(model);
		    	  scrollPane.setViewportView(table_1);
		    	  model.addColumn("TahsilatReferansNo");
		    	  model.addColumn("BorcTur");
		    	  model.addColumn("IslemTarih");
		    	  model.addColumn("BankaReferansNo");
		    	  model.addColumn("Tutar");
		    	  
		    	  try {
						doc = client1.loadXMLFromString(client1.callSoapWebService());
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        doc.getDocumentElement().normalize();
		    	  
		    	  
		    	  NodeList nnList=doc.getElementsByTagName("TahsilatDetay");
		    	  
		    	  lblNewLabel.setText(doc.getElementsByTagName("AdSoyad").item(0).getTextContent());
		    	  
		    	  for (int i = 1; i < nnList.getLength(); i++) {
		    		  Node node1=nnList.item(i);
		    		  if(node1.getNodeType()==Node.ELEMENT_NODE) {
		    		  Element elm=(Element) node1;
		    		  String r_no=elm.getElementsByTagName("TahsilatReferansNo").item(0).getTextContent();
		    		  String borc_tur=elm.getElementsByTagName("BorcTur").item(0).getTextContent();		    		  
		    		  String tarih=elm.getElementsByTagName("IslemTarih").item(0).getTextContent();
		    		  String b_r_no=elm.getElementsByTagName("BankaReferansNo").item(0).getTextContent();
		    		  String ttr=elm.getElementsByTagName("Tutar").item(0).getTextContent();
		    		  model.insertRow(model.getRowCount(), new Object [] {r_no,borc_tur,tarih,b_r_no,ttr});
		    		  }
		    		  }
		  
		      } 

			}
		});
		
	}
}
